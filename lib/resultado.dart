import 'package:flutter/material.dart';

class TelaResultado extends StatelessWidget {
  const TelaResultado({
    Key? key,
    required this.profissao,
    required this.qualidade,
  }) : super(key: key);
  final String profissao;
  final String qualidade;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Parabéns"),
      ),
      body: Center(
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                "No seu futuro na área de TI você será um ótimo $profissao $qualidade.",
                style: Theme.of(context).textTheme.headline5,
              ),
            )
          ],
        ),
      ),
    );
  }
}
