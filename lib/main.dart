import 'package:flutter/material.dart';
import 'package:profissional_ti/resultado.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
          primarySwatch: Colors.orange, typography: Typography.material2018()),
      home: const MyHomePage(title: 'Profissões de TI'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final List<MesAniversario> _mesAniversario = [
    MesAniversario(mes: "jan", profissao: "Analista de projetos"),
    MesAniversario(mes: "fev", profissao: "Arquiteto de TI"),
    MesAniversario(mes: "mar", profissao: "Programador"),
    MesAniversario(mes: "abr", profissao: "Business intelligence"),
    MesAniversario(mes: "mai", profissao: "Help desk"),
    MesAniversario(mes: "jun", profissao: "Administrador de banco de dados"),
    MesAniversario(mes: "jul", profissao: "Desenvolvedor web"),
    MesAniversario(mes: "ago", profissao: "Quality Assurance"),
    MesAniversario(mes: "set", profissao: "Cientista de dados"),
    MesAniversario(mes: "out", profissao: "Administrador de rede"),
    MesAniversario(
        mes: "nov", profissao: "Administrador de segurança de dados"),
    MesAniversario(mes: "dez", profissao: "Analista de Business Intelligence"),
  ];

  final List<Signo> _signo = [
    Signo(signo: "Áries", complemento: "da netflix"),
    Signo(signo: "Touro", complemento: "dos stories"),
    Signo(signo: "Gêmeos", complemento: "do Delivery"),
    Signo(signo: "Câncer", complemento: "do WhatsApp"),
    Signo(signo: "Leão", complemento: "da limpeza"),
    Signo(signo: "Virgem", complemento: "das receitas"),
    Signo(signo: "Libra", complemento: "do chocolate"),
    Signo(signo: "Escorpião", complemento: "da família"),
    Signo(signo: "Serpentário", complemento: "da vizinhança"),
    Signo(signo: "Sagitário", complemento: "do google"),
    Signo(signo: "Capricórnio", complemento: "de casa"),
    Signo(signo: "Aquário", complemento: "dominhoco"),
    Signo(signo: "Peixes", complemento: "do álcool em gel"),
  ];

  String? profissao;
  String? qualidade;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                "Nos diga seu mês de nascimento e signo para descubrir seu futuro na área de TI.",
                style: Theme.of(context).textTheme.headline6,
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "Selecione o mês: ",
                    style: Theme.of(context).textTheme.bodyText1,
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: DropdownButton<String>(
                        onChanged: (value) {
                          setState(() {
                            profissao = value;
                          });
                        },
                        value: profissao,
                        items: _mesAniversario.map((e) {
                          return DropdownMenuItem<String>(
                              value: e.profissao,
                              child: Text(
                                e.mes,
                                style: Theme.of(context).textTheme.bodyText2,
                              ));
                        }).toList()),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "Selecione o signo: ",
                    style: Theme.of(context).textTheme.bodyText1,
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: DropdownButton<String>(
                        onChanged: (value) {
                          setState(() {
                            qualidade = value;
                          });
                        },
                        value: qualidade,
                        items: _signo.map((e) {
                          return DropdownMenuItem<String>(
                              value: e.complemento,
                              child: Text(
                                e.signo,
                                style: Theme.of(context).textTheme.bodyText2,
                              ));
                        }).toList()),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          if (profissao != null && qualidade != null) {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => TelaResultado(
                        profissao: profissao!, qualidade: qualidade!)));
          }
        },
        tooltip: 'Increment',
        child: const Icon(Icons.add),
      ),
    );
  }
}

class MesAniversario {
  String mes;
  String profissao;
  MesAniversario({required this.mes, required this.profissao});
}

class Signo {
  String signo;
  String complemento;

  Signo({required this.signo, required this.complemento});
}
